package agh.tests;

import agh.qa.PeselValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359", true}
        };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid){
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}
